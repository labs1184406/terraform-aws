# Terraform block
terraform {
  required_version = "~> 1.3"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Providers Block
provider "aws" {
  region = "us-east-1"
}